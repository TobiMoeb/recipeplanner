﻿using RecipePlanner.Models;
using SQLite;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
using Newtonsoft.Json;

namespace RecipePlanner.Services
{
    public class DataService
    {
        private readonly FileInfo FilePath = null;

        private DirectoryInfo BackupPath => new DirectoryInfo(Path.Combine(FilePath.DirectoryName, "Backups"));

        SQLiteAsyncConnection _database;

        public DataService(string dbPath)
        {
            FilePath = new FileInfo(dbPath);

            if (!FilePath.Directory.Exists)
                Directory.CreateDirectory(FilePath.Directory.FullName);

            if (!BackupPath.Exists)
                Directory.CreateDirectory(BackupPath.FullName);

            _database = new SQLiteAsyncConnection(dbPath);
            _database.CreateTableAsync<DayPlan>().Wait();
            _database.CreateTableAsync<Recipe>().Wait();
        }

        public async Task<IDictionary<DateTime, DayPlan>> GetPlans(DateTime from, int days)
        {
            try
            {
                from = from.Date;
                DateTime to = from.AddDays(days);

                Stopwatch w = Stopwatch.StartNew();
                Debug.Print($"Getting DayPlans from {from.ToShortDateString()} to {to.ToShortDateString()}");

                var foundPlans = await _database.Table<DayPlan>()
                    .Where(p => p.DbKey >= from && p.DbKey <= to)
                    .OrderBy(p => p.DbKey)
                    .ToListAsync();

                w.Stop();

                var plans = new SortedDictionary<DateTime, DayPlan>();
                foundPlans.ForEach(p => plans[p.DbKey] = p);

                

                Debug.Print($"Found {plans.Count} DayPlans in {w.ElapsedMilliseconds}ms:\r\n{string.Join(Environment.NewLine, plans.Values)}");

                //await _database.DeleteAllAsync<DayPlan>();

                List<DayPlan> newPlans = new List<DayPlan>(0);

                for (int i = 0; i < days; i++)
                {
                    DateTime dt = from.AddDays(i).Date;

                    if (!plans.ContainsKey(dt))
                    {
                        DayPlan newPlan = new DayPlan()
                        {
                            DbKey = dt,
                        };

                        newPlans.Add(newPlan);
                        plans[dt] = newPlan;

                        Debug.Print("Created new plan for " + dt.ToShortDateString());
                    }

                    dt = dt.AddSeconds(1);

                    if (!plans.ContainsKey(dt))
                    {
                        DayPlan newPlan = new DayPlan()
                        {
                            DbKey = dt,
                        };

                        newPlans.Add(newPlan);
                        plans[dt] = newPlan;

                        Debug.Print("Created new plan for " + dt.ToShortDateString());
                    }
                }

                if (newPlans.Count > 0)
                {
                    await _database.InsertAllAsync(newPlans);
                }

                return plans;
            }
            catch(Exception ex)
            {
                Debug.Print("GetPlans failed: " + ex);
                throw;
            }
        }

        public async Task ImportRecipe(Recipe importing)
        {
            IList<Recipe> lExisting = await _database.Table<Recipe>()
                .Where(r => r.Name.ToLower() == importing.Name.ToLower())
                .ToListAsync();

            if (lExisting.Count == 0)
            {
                importing.Id = 0;

                await SaveRecipe(importing);
            }
            else
            {
                if (lExisting.Count > 1)
                {
                    Debug.Print("Found multiple recipes for " + importing.Name);
                }

                Recipe existing = lExisting.FirstOrDefault();

                if(existing.ChangedTimestamp >= importing.ChangedTimestamp)
                {
                    Console.WriteLine($"Not importing {importing.Name}. The existing recipe is newer {existing.ChangedTimestamp} > {importing.ChangedTimestamp}");
                    return;
                }

                int id = existing.Id;

                JsonConvert.PopulateObject(JsonConvert.SerializeObject(importing), existing);

                existing.Id = id;

                await SaveRecipe(existing);
            }
        }

        public class ImportProgress
        {
            public ImportProgress(string currentObject, int currentIndex, int max)
            {
                CurrentObject = currentObject;
                CurrentIndex = currentIndex;
                Max = max;
            }

            public string CurrentObject { get; set; }
            public int CurrentIndex { get; set; }
            public int Max { get; set; }
        }

        public async Task ImportDatabase(string path, IProgress<ImportProgress> progress)
        {
            if (!File.Exists(path))
                throw new FileNotFoundException($"Datei nicht gefunden: '{path}'");

            DataService importDb = new DataService(path);

            progress?.Report(new ImportProgress("Suche Rezepte", 0, 0));

            int maxCount = await importDb._database.Table<Recipe>().CountAsync();

            progress?.Report(new ImportProgress("Suche Rezepte", 0, maxCount));

            IList<Recipe> lToImport = await importDb.GetAllRecipes();

            for (int i = 0; i < lToImport.Count; i++)
            {
                Recipe import = lToImport[i];

                progress?.Report(new ImportProgress(import.Name, i + 1, maxCount));

                await ImportRecipe(import);
            }

            progress?.Report(new ImportProgress("Fertig", maxCount, maxCount));
            
        }

        public async Task Backup(string destination = null)
        {
            if (destination == null)
            {
                destination = Path.Combine(BackupPath.FullName, DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".db3");
            }
            
            await _database.BackupAsync(destination);
        }

        public IEnumerable<string> GetBackups()
        {
            foreach(var file in BackupPath.GetFiles())
            {
                if(!file.Name.Contains("Rezept"))
                {
                    yield return file.Name;
                }
            }
        }

        public async Task RestoreBackup(string file = null)
        {
            if (file == null)
                file = GetBackups().LastOrDefault();

            await _database.CloseAsync();

            FileInfo backup = new FileInfo(Path.Combine(BackupPath.FullName, file));

            File.Copy(backup.FullName, FilePath.FullName, true);

            _database = new SQLiteAsyncConnection(FilePath.FullName);
        }

        public async Task DeleteRecipe(Recipe r)
        {
            await _database.DeleteAsync(r);
        }

        public async Task<IList<Recipe>> GetAllRecipes()
        {
            return await _database.Table<Recipe>().ToListAsync();
        }

        public async Task<Recipe> GetRecipe(int id)
        {
            return await _database.FindAsync<Recipe>(id);
        }

        public async Task SaveRecipe(Recipe recipe)
        {
            if(recipe.Id == 0)
            {
                await _database.InsertAsync(recipe);
            }
            else
            {
                if(recipe.Modified)
                {
                    recipe.ChangedTimestamp = DateTime.Now;
                }
                else
                {
                    Debug.Print("Saving unchanged recipe: " + recipe.Name);
                }

                await _database.UpdateAsync(recipe);
            }
        }

        public async Task SavePlan(DayPlan plan)
        {
            await _database.UpdateAsync(plan);
        }
    }
}
