﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

[assembly: Dependency(typeof(RecipePlanner.Services.FilePathConverter))]
namespace RecipePlanner.Services
{
    public class FilePathConverter : IFilePathConverter
    {
        string IFilePathConverter.GetRealPath(string path)
        {
            return path;
        }
    }
}
