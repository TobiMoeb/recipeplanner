﻿using RecipePlanner.Models;
using SQLite;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;

namespace RecipePlanner.Services
{
    public class DataService
    {
        private readonly FileInfo FilePath = null;

        SQLiteAsyncConnection _database;

        public DataService(string dbPath)
        {
            FilePath = new FileInfo(dbPath);

            if (!FilePath.Directory.Exists)
                Directory.CreateDirectory(FilePath.Directory.FullName);

            _database = new SQLiteAsyncConnection(dbPath);
            _database.CreateTableAsync<DayPlan>().Wait();
            _database.CreateTableAsync<Recipe>().Wait();
        }

        public async Task<IDictionary<DateTime, DayPlan>> GetPlans(DateTime from, int days)
        {
            try
            {
                from = from.Date;
                DateTime to = from.AddDays(days);

                Stopwatch w = Stopwatch.StartNew();
                Debug.Print($"Getting DayPlans from {from.ToShortDateString()} to {to.ToShortDateString()}");

                var foundPlans = await _database.Table<DayPlan>()
                    .Where(p => p.Date >= from && p.Date <= to)
                    .OrderBy(p => p.Date)
                    .ToListAsync();

                w.Stop();

                var plans = new SortedDictionary<DateTime, DayPlan>();
                foundPlans.ForEach(p => plans[p.Date] = p);

                

                Debug.Print($"Found {plans.Count} DayPlans in {w.ElapsedMilliseconds}ms:\r\n{string.Join(Environment.NewLine, plans.Values)}");

                //await _database.DeleteAllAsync<DayPlan>();

                List<DayPlan> newPlans = new List<DayPlan>(0);

                for (int i = 0; i < days; i++)
                {
                    DateTime dt = from.AddDays(i).Date;

                    if (!plans.ContainsKey(dt))
                    {
                        DayPlan newPlan = new DayPlan()
                        {
                            Date = dt,
                        };

                        newPlans.Add(newPlan);
                        plans[dt] = newPlan;

                        Debug.Print("Created new plan for " + dt.ToShortDateString());
                    }
                    else
                    {

                    }
                }

                if (newPlans.Count > 0)
                {
                    await _database.InsertAllAsync(newPlans);
                }

                return plans;
            }
            catch(Exception ex)
            {
                Debug.Print("GetPlans failed: " + ex);
                throw;
            }
        }

        internal async Task Backup(string destination = null)
        {
            if (destination == null)
            {
                destination = Path.Combine(FilePath.DirectoryName, DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".db3");
            }
            
            await _database.BackupAsync(destination);
        }

        public IEnumerable<string> GetBackups()
        {
            foreach(var file in FilePath.Directory.GetFiles())
            {
                if(!file.Name.Contains("Rezept"))
                {
                    yield return file.Name;
                }
            }
        }

        public async Task RestoreBackup(string file = null)
        {
            if (file == null)
                file = GetBackups().LastOrDefault();

            await _database.CloseAsync();

            FileInfo backup = new FileInfo(Path.Combine(FilePath.DirectoryName, file));

            File.Copy(backup.FullName, FilePath.FullName, true);

            _database = new SQLiteAsyncConnection(FilePath.FullName);
        }

        internal async Task DeleteRecipe(Recipe r)
        {
            await _database.DeleteAsync(r);
        }

        internal async Task<IList<Recipe>> GetAllRecipes()
        {
            return await _database.Table<Recipe>().ToListAsync();
        }

        public async Task<Recipe> GetRecipe(int id)
        {
            return await _database.FindAsync<Recipe>(id);
        }

        public async Task SaveRecipe(Recipe recipe)
        {
            if(recipe.Id == 0)
            {
                await _database.InsertAsync(recipe);
            }
            else
            {
                await _database.UpdateAsync(recipe);
            }
        }

        public async Task SavePlan(DayPlan plan)
        {
            await _database.UpdateAsync(plan);
        }
    }
}
