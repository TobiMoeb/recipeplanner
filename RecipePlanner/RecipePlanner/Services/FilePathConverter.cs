﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RecipePlanner.Services
{
    public interface IFilePathConverter
    {
        string GetRealPath(string path);
    }
}
