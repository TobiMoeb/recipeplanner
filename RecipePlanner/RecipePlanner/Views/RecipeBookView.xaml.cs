﻿using RecipePlanner.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RecipePlanner.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RecipeBookView : ContentPage
    {
        private static RecipeBookView _cachedPage = null;

        public static RecipeBookView ViewInstance
        {
            get
            {
                if (_cachedPage == null)
                    _cachedPage = new RecipeBookView();

                return _cachedPage;
            }
            set
            {
                _cachedPage = value;
            }
        }

        public ObservableCollection<RecipeModel> Items { get; set; } = null;
        public ObservableCollection<RecipeModel> FilteredItems { get; set; } = null;

        public class RecipeModel : INotifyPropertyChanged
        {
            public RecipeModel(Recipe recipe)
            {
                Recipe = recipe;
            }

            public event PropertyChangedEventHandler PropertyChanged
            {
                add
                {
                    ((INotifyPropertyChanged)Recipe).PropertyChanged += value;
                }

                remove
                {
                    ((INotifyPropertyChanged)Recipe).PropertyChanged -= value;
                }
            }

            public Recipe Recipe { get; }

            public int Id => Recipe.Id;

            public string Name => Recipe.Name;
            public string Description => Recipe.Description;
            public string Tags => Recipe.Tags;
            public string Ingredients => Recipe.Ingredients;
            public string Preparation => Recipe.Preparation;

            private ImageSource _imgSource = null;

            public ImageSource ImageSource
            {
                get
                {
                    if (_imgSource == null)
                    {
                        if (Recipe.ImageDate != null)
                        {
                            _imgSource = ImageSource.FromStream(() => new MemoryStream(Recipe.ImageDate));
                        }
                        else
                        { 
                        }
                    }
                    else
                    {

                    }

                    return _imgSource;
                }
            }
        }

        public RecipeBookView()
        {
            
            InitializeComponent();

            SizeChanged += RecipeBookView_SizeChanged;
            this.Appearing += RecipeBookView_Appearing;
        }

        private void RecipeBookView_SizeChanged(object sender, EventArgs e)
        {
            //MyListView.ItemsLayout = new GridItemsLayout((int)(Width / 200), ItemsLayoutOrientation.Vertical);
            ((GridItemsLayout)MyListView.ItemsLayout).Span = (int)(Width / 300);
        }

        public static void AddSorted(IList<RecipeModel> list, RecipeModel item)
        {
            int i = 0;
            while (i < list.Count && string.CompareOrdinal(list[i]?.Name, item?.Name) < 0)
                i++;

            list.Insert(i, item);
        }

        private async void RecipeBookView_Appearing(object sender, EventArgs e)
        {
            if (Items != null)
                return;

            IList<Recipe> recipes = await App.Database.GetAllRecipes();

            Items = new ObservableCollection<RecipeModel>();

            foreach(var recipe in recipes.OrderBy(r => r.Name))
            {
                Items.Add(new RecipeModel(recipe));
            }

            MyListView.ItemsSource = Items;
        }

        async void Handle_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item == null)
                return;

            RecipeModel r = (RecipeModel)e.Item;

            string previous = r.Name;

            var page = new RecipeDetailView(r.Recipe);

            page.Disappearing += (_, __) =>
            {
                if (r.Name != previous)
                {
                    Items.Remove(r);

                    AddSorted(Items, r);
                }
            };

            await Navigation.PushAsync(page);

            //Deselect Item
            ((ListView)sender).SelectedItem = null;
        }

        private async void NewRecipe_Clicked(object sender, EventArgs e)
        {
            Recipe r = new Recipe();

            var page = new RecipeDetailView(r);

            page.Disappearing += (_, __) =>
            {
                if (r.Id != 0)
                {
                    AddSorted(Items, new RecipeModel(r));
                }
            };

            await Navigation.PushAsync(page);
        }

        private RecipeModel SenderContext(object sender)
        {
            RecipeModel r = (RecipeModel)((BindableObject)sender).BindingContext;
            return r;
        }

        private async void Delete_Clicked(object sender, EventArgs e)
        {
            RecipeModel r = SenderContext(sender);

            bool delete;

            if (!string.IsNullOrEmpty(r.Name))
            {
                delete = await DisplayAlert("Löschen", $"'{r.Name}' wirklich löschen?", "Löschen", "Abbrechen");
            }
            else
            {
                delete = true;
            }

            if (delete)
            {
                Items.Remove(r);

                if (MyListView.ItemsSource == FilteredItems)
                {
                    FilteredItems?.Remove(r);
                }

                _ = App.Database.DeleteRecipe(r.Recipe);
            }
        }

        public Action<Recipe> OnRecipeSelected = null;

        private async void Select_Clicked(object sender, EventArgs e)
        {
            RecipeModel r = SenderContext(sender);

            OnRecipeSelected?.Invoke(r.Recipe);

            await Navigation.PopModalAsync();

            //Reset Search
            Search("");
            SearchEntry.Text = "";
        }

        private async void Cancle_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }

        private void MyListView_ItemAppearing(object sender, ItemVisibilityEventArgs e)
        {
            ListView list = (ListView)sender;
        }

        private async void MyListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.CurrentSelection.Count == 0)
                return;

            //Deselect Item
            ((CollectionView)sender).SelectedItem = null;
            ((CollectionView)sender).SelectedItems = null;

            RecipeModel r = (RecipeModel)e.CurrentSelection[0];

            string previous = r.Name;

            var page = new RecipeDetailView(r.Recipe);

            page.Disappearing += (_, __) =>
            {
                if (r.Name != previous)
                {
                    Items.Remove(r);
                    AddSorted(Items, r);
                }
            };

            await Navigation.PushAsync(page);


        }

        private void Search_Accepted(object sender, EventArgs e)
        {
            Search(SearchEntry.Text);
        }

        private void SearchButton_Clicked(object sender, EventArgs e)
        {
            Search(SearchEntry.Text);
        }

        private void Search(string strSearchTerm)
        {
            if (!string.IsNullOrWhiteSpace(strSearchTerm))
            {
                FilteredItems = new ObservableCollection<RecipeModel>();

                foreach (var item in Items)
                {
                    if (item.Name.Contains(strSearchTerm, StringComparison.InvariantCultureIgnoreCase))
                    {
                        FilteredItems.Add(item);
                    }
                }

                MyListView.ItemsSource = FilteredItems;
            }
            else
            {
                MyListView.ItemsSource = Items;
            }
        }
    }

    public static class Extensions
    {
        public static bool Contains(this string text, string value, StringComparison stringComparison)
        {
            return text.IndexOf(value, stringComparison) >= 0;
        }
    }
}
