﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static RecipePlanner.Services.DataService;

namespace RecipePlanner.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ImportView : ContentPage, IProgress<ImportProgress>
    {
        private readonly string _source;

        private Task _importTask = null;

        private readonly Action _onFinish;

        public ImportView(string importSource, Action onFinish)
        {
            _source = importSource;
            _onFinish = onFinish;
            InitializeComponent();
            Lbl_ImportingFrom.Text = "Importiere von: " + importSource;
        }

        private void Import()
        {
            App.Database.ImportDatabase(_source, this).Wait();
        }

        public void Report(ImportProgress value)
        {
            Device.BeginInvokeOnMainThread(() => ReportUI(value));
        }

        private void ReportUI(ImportProgress value)
        {
            Lbl_CurrentName.Text = value.CurrentObject;
            Lbl_Progress.Text = value.CurrentIndex + "/" + value.Max;

            if(value.Max > 0)
                MyProgressBar.Progress = ((double)value.CurrentIndex / (double)value.Max);
        }

        protected override void OnAppearing()
        {
            if (_importTask == null)
            {
                _importTask = Task.Run(Import)
                    .ContinueWith(TaskDone);
            }

            base.OnAppearing();
        }

        private void TaskDone(Task arg1)
        {
            if (arg1.Exception == null)
            {
                _onFinish?.Invoke();
            }
            else
            {
                Report(new ImportProgress("Fehler: " + arg1.Exception, 0, 0));
            }
        }
    }
}