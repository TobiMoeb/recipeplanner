﻿using RecipePlanner.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RecipePlanner.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DayPlanEdit : ContentPage
    {
        private DayPlan Item => (DayPlan)BindingContext;

        private string PreviousEntry = "";

        public DayPlanEdit()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            PreviousEntry = Item.RecipeText;
        }

        private async void OnAbortButton_Clicked(object sender, EventArgs e)
        {
            Item.RecipeText = PreviousEntry;

            await Navigation.PopModalAsync();
        }

        private async void OnSaveButton_Clicked(object sender, EventArgs e)
        {
            Item.EntryType = DayPlanEntryType.FreeHand;

            _ = App.Database.SavePlan(Item)
                .ContinueWith(OnSaveDone);

            await Navigation.PopModalAsync();
        }

        private void OnSaveDone(Task arg1)
        {
            if (arg1.IsFaulted)
            {
                Debug.Print("Save failed: " + arg1.Exception.ToString());
            }
            else
            {
                Debug.Print("Saved: " + Item);
            }
        }
    }
}