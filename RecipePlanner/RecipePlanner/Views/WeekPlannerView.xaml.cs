﻿using Plugin.FilePicker;
using Plugin.FilePicker.Abstractions;
using RecipePlanner.Models;
using RecipePlanner.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RecipePlanner.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WeekPlannerView : ContentPage
    {
        private int _dayOffset = 0;

        public WeekPlannerView()
        {
            InitializeComponent();

            DisplayData();
        }

        private async void DisplayData()
        {
            var plans = new List<DayPlan>();

            var dbPlans = await App.Database.GetPlans(DateTime.Now.AddDays(_dayOffset), 7);

            foreach(var plan in dbPlans)
            {
                plans.Add(plan.Value);
            }

            listView.ItemsSource = plans;
        }

        protected override void OnAppearing()
        {
            
            base.OnAppearing();
        }

        private void BackButton_Clicked(object sender, EventArgs e)
        {
            --_dayOffset;
            DisplayData();
        }

        private void TodayButton_Clicked(object sender, EventArgs e)
        {
            if (_dayOffset == 0)
                return;

            _dayOffset = 0;
            DisplayData();
        }

        private void ForwardButton_Clicked(object sender, EventArgs e)
        {
            ++_dayOffset;
            DisplayData();
        }

        private void FreeEdit_Clicked(object sender, EventArgs e)
        {
            DayPlan plan = GetSenderContext(sender);

            DisplayFreeEdit(plan);
        }

        private async void DisplayFreeEdit(DayPlan plan)
        {
            await Navigation.PushModalAsync(
                //new NavigationPage(
                new DayPlanEdit
                    {
                        BindingContext = plan
                    });
            //);
        }

        private static DayPlan GetSenderContext(object sender)
        {
            return (DayPlan)((BindableObject)sender).BindingContext;
        }

        private async void PickRecipe_Clicked(object sender, EventArgs e)
        {
            DayPlan plan = GetSenderContext(sender);

            await PickRecipe(plan);
        }

        private async Task PickRecipe(DayPlan plan)
        {
            var page = RecipeBookView.ViewInstance;
            page.OnRecipeSelected = recipe =>
            {
                plan.EntryType = DayPlanEntryType.Recipe;
                plan.RecipeId = recipe.Id;
                plan.RecipeText = recipe.Name;
                _ = App.Database.SavePlan(plan);
            };

            await Navigation.PushModalAsync(new NavigationPage(page));
        }

        private void listView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            DayPlan plan = (DayPlan)e.Item;

            switch(plan.EntryType)
            {
                case DayPlanEntryType.FreeHand:
                    DisplayFreeEdit(plan);
                    break;
                case DayPlanEntryType.Undefined:
                case DayPlanEntryType.Recipe:
                default:
                    if (plan.RecipeId != 0)
                    {
                        DisplayRecipeDetails(plan);
                    }
                    else
                    {
                        _ = PickRecipe(plan);
                    }
                    break;
            }

            //Deselect Item
            ((ListView)sender).SelectedItem = null;
        }

        private async void DisplayRecipeDetails(DayPlan plan)
        {
            var r = await App.Database.GetRecipe(plan.RecipeId);

            if (r == null || r.Id == 0)
                return;

            string previous = r.Name;

            var page = new RecipeDetailView(r);

            page.Disappearing += (_, __) =>
            {
                if (r.Name != previous)
                {
                    plan.RecipeText = r.Name;
                }
            };

            await Navigation.PushAsync(page);
        }

        private void Label_SizeChanged(object sender, EventArgs e)
        {
        }

        private async void RestoreBackup_Clicked(object sender, EventArgs e)
        {
            MenuItem item = (MenuItem)sender;
            item.IsEnabled = false;

            bool accept = await DisplayAlert("Wiederherstellen", $"Die aktuelle Datei wird hierbei gelöscht", "Wiederherstellen", "Abbrechen");

            if (accept)
            {
                await App.Database.RestoreBackup();

                DisplayData();
            }

            item.IsEnabled = true;
        }

        private async void OnBackup_Clicked(object sender, EventArgs e)
        {
            MenuItem item = (MenuItem)sender;
            item.IsEnabled = false;

            await App.Database.Backup();

            item.IsEnabled = true;
        }


        private void ShowFolder_Clicked(object sender, EventArgs e)
        {
            DisplayAlert("Dateipfad", new FileInfo(App.FilePath).DirectoryName, "ok");
        }

        private async void Import_Clicked(object sender, EventArgs e)
        {
            try
            {
                ImportButton.IsEnabled = false;

                FileData file = await CrossFilePicker.Current.PickFile();

                if (file == null)
                    return;

                if(!file.FileName.EndsWith(".db3"))
                {
                    await DisplayAlert("Fehler", "Es muss eine .db3-Datei gewält werden", "Ok");
                    return;
                }
                var service = DependencyService.Get<IFilePathConverter>();

                string path = DependencyService.Get<IFilePathConverter>().GetRealPath(file.FilePath);

                ImportView import = new ImportView(path, () => RecipeBookView.ViewInstance = null);

                await Navigation.PushAsync(import);
            }
            catch(Exception ex)
            {
                await DisplayAlert("Fehler", ex.Message, "Ok");
            }
            finally
            {
                ImportButton.IsEnabled = true;
            }
        }
    }
}