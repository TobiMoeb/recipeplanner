﻿using RecipePlanner.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RecipePlanner.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RecipeDetailView : ContentPage
    {
        private readonly string _intialDatal;

        public RecipeDetailView(Recipe recipe)
        {
            InitializeComponent();
            _intialDatal = JsonConvert.SerializeObject(recipe);
            BindingContext = Recipe = recipe;
        }

        public Recipe Recipe { get; private set; }

        private async void Cancle_Clicked(object sender, EventArgs e)
        {
            JsonConvert.PopulateObject(_intialDatal, Recipe);

            await Navigation.PopAsync();
        }

        private async void Save_Clicked(object sender, EventArgs e)
        {
            SaveButton.IsEnabled = false;
            await App.Database.SaveRecipe(Recipe);

            await Navigation.PopAsync();
        }
    }
}