﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace RecipePlanner.Models
{
    public enum DayPlanEntryType
    {
        Undefined = 0,
        Recipe = 1,
        FreeHand = 2
    }

    public class DayPlan : BaseModel
    {
        private DateTime _date = DateTime.Now;

        [PrimaryKey]
        public DateTime Date
        {
            get { return _date.Date; }
            set { SetProperty(ref _date, value.Date); }
        }

        string _recipe = "";
        public string RecipeText
        {
            get { return _recipe; }
            set { SetProperty(ref _recipe, value); }
        }

        private int _recipeId = 0;

        public int RecipeId
        {
            get { return _recipeId; }
            set { SetProperty(ref _recipeId, value); }
        }

        private DayPlanEntryType _entryType = DayPlanEntryType.Undefined;

        public DayPlanEntryType EntryType
        {
            get { return _entryType; }
            set { SetProperty(ref _entryType, value); }
        }

        public DayOfWeek DayOfWeek => Date.DayOfWeek;

        public string DayOfWeekName => DateTimeFormatInfo.CurrentInfo.GetDayName(this.DayOfWeek);

        public string DisplayColor
        {
            get
            {
                if (Date == DateTime.Now.Date)
                {
                    return "CornflowerBlue";
                }
                else
                {
                    return "#eee";
                }
            }
        }

        public override string ToString()
        {
            return $"{Date.ToShortDateString()}: {RecipeText}";
        }
    }
}
