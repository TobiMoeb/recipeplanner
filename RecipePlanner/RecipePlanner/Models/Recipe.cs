﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace RecipePlanner.Models
{
    public class Recipe : BaseModel
    {
        private int _id;

        [PrimaryKey]
        [AutoIncrement]
        public int Id
        {
            get { return _id; }
            set { SetProperty(ref _id, value); }
        }

        private string _name = "";

        public string Name
        {
            get { return _name; }
            set { SetProperty(ref _name, value); }
        }

        private string _description = "";

        public string Description
        {
            get { return _description; }
            set { SetProperty(ref _description, value); }
        }

        private string _tags = "";

        public string Tags
        {
            get { return _tags; }
            set { SetProperty(ref _tags, value); }
        }

        private string _ingredients = "";

        public string Ingredients
        {
            get { return _ingredients; }
            set { SetProperty(ref _ingredients, value); }
        }

        private string _preparation;

        public string Preparation
        {
            get { return _preparation; }
            set { SetProperty(ref _preparation, value); }
        }

        private byte[] _imageData;

        public byte[] ImageDate
        {
            get { return _imageData; }
            set 
            { 
                SetProperty(ref _imageData, value); 
            }
        }
    }
}
