﻿using RecipePlanner.Services;
using RecipePlanner.Views;
using System;
using System.Diagnostics;
using System.IO;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RecipePlanner
{
    public partial class App : Application
    {
        public static string FilePath { get; set; } 

        static DataService database;

        public static DataService Database
        {
            get
            {
                if (database == null)
                {
                    string strPath = FilePath;
                    Debug.Print("Using storage: " + strPath);
                    database = new DataService(strPath);
                }
                return database;
            }
        }

        public App()
        {
#if DEBUG
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("de-de");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("de-de");
#endif

            InitializeComponent();

            MainPage = new NavigationPage(new WeekPlannerView());
            //MainPage = new NavigationPage(new RecipeDetailView(null));
            //MainPage = new NavigationPage(new RecipeBookView());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
