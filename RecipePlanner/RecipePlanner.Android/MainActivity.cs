﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.IO;
using RecipePlanner.Services;

namespace RecipePlanner.Droid
{
    [Activity(Label = "Rezepte", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        private const int PermissionCallbackId = 99;
        private bool started = false;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            FilePathConverter.Context = BaseContext;
            FilePathConverter.ContentResolver = ContentResolver;
            var path = Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryDocuments);

            RecipePlanner.App.FilePath = Path.Combine(path.AbsolutePath, "Rezepte", "Rezepte.db3");

            if(ApplicationContext.CheckSelfPermission(Android.Manifest.Permission.ReadExternalStorage) != Permission.Granted ||
               ApplicationContext.CheckSelfPermission(Android.Manifest.Permission.WriteExternalStorage) != Permission.Granted)
            {
                RequestPermissions(
                    new string[] { Android.Manifest.Permission.ReadExternalStorage, 
                                   Android.Manifest.Permission.WriteExternalStorage },
                    PermissionCallbackId);
            }

            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;
            
            base.OnCreate(savedInstanceState);

            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);

            if (ApplicationContext.CheckSelfPermission(Android.Manifest.Permission.ReadExternalStorage) == Permission.Granted ||
                ApplicationContext.CheckSelfPermission(Android.Manifest.Permission.WriteExternalStorage) == Permission.Granted)
            {
                StartApplication();
            }
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            if(!started)
            {
                if (ApplicationContext.CheckSelfPermission(Android.Manifest.Permission.ReadExternalStorage) == Permission.Granted ||
                    ApplicationContext.CheckSelfPermission(Android.Manifest.Permission.WriteExternalStorage) == Permission.Granted)
                {
                    StartApplication();
                }
            }
        }

        private void StartApplication()
        {
            started = true;

            FileInfo file = new FileInfo(App.FilePath);

            if (!file.Directory.Exists)
                file.Directory.Create();

            LoadApplication(new App());
        }
    }
}